#!/usr/bin/env perl
use v5.16;
use LambdaTerm;
use Devel::Gladiator qw(arena_table);
use Marpa::R2;

# Grammar is a little ambiguous; two terms next to each other is bad
# Marpa does it right though
my $grammar = <<'End BNF Grammar;';
	:default ::= action => [name,values]
	lexeme default = latm => 1
	:start ::= Program
	Program ::=
		Expression
	|	Expression (statementsep)
	|	(Program) Expression
	|	(Program) Expression (statementsep)
	|	(Program) (statementsep) Expression
	|	(Program) (statementsep) Expression (statementsep)
	Expression ::=
		AssignExpression
	|	LetExpression
			action => run
	AssignExpression ::=
			(let) VarName (assign) LetExpression
				action => registerSymbol
		|	(let) FunDecl (assign) LetExpression
				action => registerFunSymbol
	LetExpression ::=
			LambdaTerm
				action => ::first
		|	(let) VarName (equals) LambdaTerm (in) LetExpression 
				action => letSub
		|	(let bgroup) VarName (equals) LambdaTerm (egroup in) LetExpression
				action => letSub
		|	(let) FunDecl (equals) LambdaTerm (in) LetExpression
				action => letFunSub
		|	(let bgroup) FunDecl (equals) LambdaTerm (egroup in) LetExpression
				action => letFunSub
	FunDecl ::=
			FunName (bgroup) VarList (egroup)
				action => declFun
	FunInst ::=
			FunName (bgroup) LambdaList (egroup)
				action => makeFun
	VarList ::=
			VarName 
				action => buildList
		|	VarName (listsep) VarList
				action => buildList
	LambdaList ::=
			LambdaTerm 
				action => buildList
		|	LambdaTerm (listsep) LambdaList 
				action => buildList
	LambdaTerm ::=
			DotFreeTerm 
				action => ::first
		|	(dot) LambdaTerm 
				action => ::first
		|	DotFreeTerm (dot) LambdaTerm 
				action => makeApp
		|	DottedAbstraction
				action => ::first
		|	DotFreeTerm DottedAbstraction
				action => makeApp
	DottedAbstraction ::=
			(lambda) VarName (dot) LambdaTerm
				action => makeLamb
		|	(lambda) VarName DottedAbstraction
				action => makeLamb
	DotFreeTerm ::=
			Atom 
				action => ::first
		|	DotFreeTerm Atom 
				action => makeApp
	Atom ::=
			VarName 
				action => ::first
		|	FunInst 
				action => ::first
		|	(bgroup) LambdaTerm (egroup) 
				action => ::first
		|	(lambda) VarName Atom 
				action => makeLamb
	VarName ::=
			lowercaseword
				action => makeVar
	FunName ::=
			capitalword
				action => ::first
	:discard ~ whitespace
	:discard ~ singlelinecomment
	:discard ~ multilinecomment
	whitespace ~ [\s]+
	let ~ 'let'
	in ~ 'in'
	equals ~ '='
	assign ~ ':='
	bgroup ~ '('
	egroup ~ ')'
	dot ~ '.'
	listsep ~ ','
	statementsep ~ ';'
	word ~ [\w]*
	capitalword ~ [A-Z]word
	lowercaseword ~ [a-z]word
	notnewline ~ [^\n]*
	newline ~ [\n]
	anything ~ [\s\S]*
	singlelinecommentsymbol ~ '//'
	blockcommentbegin ~ '/*'
	blockcommentend ~ '*/'
	singlelinecomment ~ singlelinecommentsymbol notnewline newline
	multilinecomment ~ blockcommentbegin anything blockcommentend
	lambda ~ [\x{ce}\x{BB}\\]
End BNF Grammar;

{
	my $t = Marpa::R2::Scanless::G->new( { source => \$grammar } );
	my $r = Marpa::R2::Scanless::R->new( { grammar => $t, semantics_package => 'Actions'} );

	my $string;
	{ local $/; $string = <>; }
	$string =~ s/λ/\\/g;
	$r->read(\$string);
	$r->value;
	undef($r);
}


###########################################

package Actions;
no  warnings qw/experimental/;
use Term::ANSIColor;
use Data::Dumper qw/Dumper/;
use Storable qw/dclone/;

our %symtab;

$Data::Dumper::Sortkeys = \&my_filter;
sub printDumper {
	shift;
	my $term = shift;
	my @redexes;
	my $choice;
	do {
		@redexes = $term->redexes;
		#print Dumper($term);
		print $term->string(), "\n";
		
		my $i;
		foreach (@redexes) {
			say ++$i, ": ", $_->string;
		}
	} while (chomp($choice = <STDIN>) && $redexes[$choice-1] && $redexes[$choice-1]->contract($term))
	#print $term->redexes;
}

sub run {
	shift;
	my $term = shift;
	my @redexes;
	my $choice = 0;
	BEGIN: {
		@redexes = $term->redexes;
		#say ::arena_table();
		if ($choice eq 'normal') {
			if (@redexes) {
				$term = $redexes[0]->contractRedex($term);
				goto BEGIN if @redexes;
			}
		}
		say $term->string();
		my $i;
	if ($choice =~ /\d+/) {
		foreach (@redexes) {
			say ++$i, ": ", (@{$_->{left}{bound_vars}}?color('bold blue'):color('bold green')), '(', $_->{left}->string, ')', color('bold red'), '(', $_->{right}->string, ')', color('reset');
		}
	}
	chomp($choice = <STDIN>);
	goto END if $choice eq 'q';
	if ($choice eq 'p') {
		print Dumper($term);
		goto BEGIN;
	}
	elsif ($choice eq 'normal' or ($choice eq "")  && ($choice = 'normal')) {
		goto BEGIN;
	} elsif ($choice eq 'm') {
		print ::arena_table();
		goto BEGIN;
	} elsif ($choice =~ /^eval (.*)$/) {
		eval $1;
		goto BEGIN;
	} elsif ($choice =~ /(\d+)$/) {
		say "Reducing $choice...";
		if ($redexes[$1 - 1]) {
			$term = $redexes[$choice-1]->contractRedex($term);
		}
		goto BEGIN;
	}
	} END:
	 say $term->string;
	 undef(@redexes);
	 undef($term);
}

sub my_filter {
	my ($hash) = @_;
	local $, = " ";
	if ($hash->{type}) {
		my @return = ('type');
		given($hash->{type}) {
			when ('Var') {
				push @return, 'name';
				push @return, 'bound_at' if defined($hash->{bound_at});
			}
			when ('Fun') {
				push @return, qw(name arity list);
			}
			when ('Abs') {
				push @return, qw(term bound_vars);
			}
			when ('App') {
				push @return, qw(left right);
			}
		}
		push @return, 'fv' if defined($hash->{fv});
		push @return, 'parent' if defined($hash->{parent});
		return \@return;
	} else {
		return [sort keys %$hash];
	}
}

sub letSub {
	my (undef, $letVar, $equalTerm, $inData) = @_;
	my $t = LambdaTerm->new('App', LambdaTerm->new('Abs', $letVar, $inData), $equalTerm);
	$t = $t->contractRedex();
	#my $loc = $inData->{fv}{$letVar->{name}};
	#if ($loc && @$loc) {
	#	while (@$loc) {
	#		my $hole = pop $loc;
	#		my $copyEqualTerm = $equalTerm->cloneWithSideEffects($inData);
	#		$inData = $hole->substituteInPlace($inData, $copyEqualTerm);
	#	}
	#}
	return $inData;
}
sub letFunSub {
	my (undef, $funDecl, $equalTerm, $inData) = @_;
	my $funName = $funDecl->[0];
	my $varList = $funDecl->[1];
	makeMetaFunctionOverTerm($funDecl, $equalTerm);
	delete($equalTerm->{fv});
	my $funLoc = $inData->{fv}{$funName};
	if ($funLoc && @$funLoc) {
		# * loop through function locations.
		 while (@$funLoc) {
			my $hole = pop $funLoc;
			# * make a copy of abstraction we made (side effect: register free variables)
			my $copyEqualTerm = $equalTerm->cloneWithSideEffects($inData);
			# * using $inData's free variables, so those pointers get altered during contraction
			$copyEqualTerm->{fv} = $inData->{fv};
			my $instList = $hole->{list};
			$copyEqualTerm = instantiateMetaFunction($copyEqualTerm, $instList);
			foreach my $term (@$instList) {
				# * apply the abstraction to the instantiation list
				$copyEqualTerm = LambdaTerm->new('App', $copyEqualTerm, $term);
				$copyEqualTerm = $copyEqualTerm->contractRedex();
			}
			# * only head terms have free variables, and copyEqualTerm not necc. head.
			delete($copyEqualTerm->{fv});
			# * substitute the term we have for the place of the hole
			$inData = $hole->substituteInPlace($inData, $copyEqualTerm);
		}
	}
	delete($inData->{fv}{$funName});
	return $inData;
}

sub declFun {
	my (undef, $funName, $varList) = @_;
	return [$funName, $varList];
}

sub buildList {
	my (undef, $name, $list) = @_;
	if ($list) {
		unshift @$list, $name;	
		return $list;
	}
	return [$name];
}

sub registerSymbol {
	my (undef,$var, $term) = @_;
	our %symtab;
	$symtab{$var->{name}} = [$var, $term];
}
sub registerFunSymbol {
	my (undef, $funDecl, $term) = @_;
	our %symtab;
	$symtab{$funDecl->[0]} = [$funDecl, $term];
}


##

sub makeMetaFunctionOverTerm {
	my ($funDecl, $term) = @_;
	foreach (reverse @{$funDecl->[1]}) {
		$term = LambdaTerm->new('Abs', $_, $term);
	}
	return $term;
}
sub instantiateMetaFunction {
	my ($function, $list) = @_;
	foreach my $term (@$list) {
		$function = LambdaTerm->new('App', $function, $term);
		$function = $function->contractRedex();
	}
	return $function;
}

sub makeFun {
	my (undef, $funName, $argList) = @_;
	our %symtab;
	if ($symtab{$funName}) {
		my $function = 
			makeMetaFunctionOverTerm($symtab{$funName}[0], dclone($symtab{$funName}[1]));
		$function = instantiateMetaFunction($function, $argList);
		return $function;
	}
	LambdaTerm->new("Fun", $funName, $argList);
}
sub makeVar {
	my (undef, $varName) = @_;
	our %symtab;
	if ($symtab{$varName}) {
		return dclone($symtab{$varName}[1]);
	}
	LambdaTerm->new("Var", $varName);
}
sub makeLamb {
	my (undef, $varName, $term) = @_;
	LambdaTerm->new("Abs", $varName, $term);
}
sub makeApp {
	my (undef, $left, $right) = @_;
	LambdaTerm->new("App", $left, $right);
}
