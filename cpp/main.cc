#include <forward_list>
#include <list>
#include <string>
#include <iostream>
#include "parser.tab.hh"
#include "lexer_class.hh"

using namespace std;
using flyweight=typename boost::flyweight<string>;

int main() {
    /*
    auto a = Term{flyweight{"Hello"}};
    auto b = Term{flyweight{"Hello"}, move(a)};
    auto c = Term{flyweight{"a"}};
    auto d = Term{move(b), move(c)};
    d.contract_redex(d.term->ptr());
    cout << 1 << endl;
    */
	FlexBison::Lexer scanner { &cin , &cout};
	FlexBison::Parser parser {scanner};
	parser.parse();
    //*/
}

