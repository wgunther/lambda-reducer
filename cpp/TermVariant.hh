#include <string>
#include <iostream>
#include <memory>
#include <forward_list>
#include <deque>
#include <boost/flyweight.hpp>
#include <utility>
#include <cassert>

struct TermVariant;
struct Var;
struct App;
struct Abs;

struct Var {
	template <typename T>
		using Ptr = typename std::unique_ptr<T>; 
	using string = typename boost::flyweight<std::string>;
	//
	const string name;
	TermVariant* bound_at = nullptr;
	//**********
	Var(const string& n)
		: name{n} { }
	Var(const string&& n)
		: name{std::move(n)} { }
	Var(Var&& v)
		: name{std::move(v.name)}, bound_at{std::move(v.bound_at)} { }
	Var(const Var&) = delete;
};
struct Fun {
	template <typename T>
		using Ptr = typename std::unique_ptr<T>; 
	template <typename T>
		using List = typename std::forward_list<T>;
	using string = typename boost::flyweight<std::string>;
	//
	const string name;
	List<Ptr<TermVariant>> list;
	//**********
	Fun(const string& n)
		: name{n}, list{} { }
	Fun(const string&& n)
		: name{std::move(n)}, list{} { }
	Fun(const string& n, List<Ptr<TermVariant>>&& l)
		: name{n}, list{std::move(l)} { }
	Fun(const string&& n, List<Ptr<TermVariant>>&& l)
		: name{std::move(n)}, list{std::move(l)} { }
	Fun(Fun&& f)
		: Fun{std::move(f.name), std::move(f.list) } { }
	void set_list(List<Ptr<TermVariant>>&& l) {
		this->list = std::move(l);
	}
	Fun(const Fun&) = delete;
};
struct App {
	template <typename T>
		using Ptr = typename std::unique_ptr<T>; 
	//
	Ptr<TermVariant> left;
	Ptr<TermVariant> right;
	//**********
	App(TermVariant* l, TermVariant* r)
		: left{l}, right{r} { }
	App(App&& a)
		: App{a.left.release(), a.right.release()} { }
	App(const App&) = delete;
};

struct Abs {
	template <typename T>
		using Ptr = typename std::unique_ptr<T>; 
	template <typename T>
		using List = typename std::forward_list<T>;
	//
	List<TermVariant*> bound_vars;
	Ptr<TermVariant> abstracted_term;
	//**********
	Abs(TermVariant* t)
		: abstracted_term{t} { }
	Abs(Abs&& a)
		: bound_vars{std::move(bound_vars)}, abstracted_term{a.abstracted_term.release()} { }
	Abs(const Abs&) = delete;
};

struct TermVariant {
	template <typename T>
		using Ptr = typename std::unique_ptr<T>; 
	template <typename T>
		using List = typename std::forward_list<T>;
	//
	const union {
		Var var;
		App app;
		Abs abs;	
		Fun fun;
	};
	TermVariant* parent = nullptr;
	const enum class TermVariantType : char {
		VOID, VAR, APP, ABS, FUN
	} tag;
	using Tag = typename TermVariant::TermVariantType;
	//**********
	TermVariant (Var&& v)
		: var{std::move(v)}, tag{Tag::VAR}  { }
	TermVariant (App&& a)
		: app{std::move(a)}, tag{Tag::APP} { }
	TermVariant (Fun&& f)
		: fun{std::move(f)}, tag{Tag::FUN} { }
	TermVariant (Abs&& a)
		: abs{std::move(a)}, tag{Tag::ABS} { }
	TermVariant (const TermVariant& t) = delete;
	TermVariant (TermVariant&& t) = delete;
	~TermVariant () {
        switch(this->tag) {
            case Tag::VOID:
                break;
            case Tag::VAR:
                this->var.~Var();
                break;
			case Tag::FUN:
				this->fun.~Fun();
				break;
            case Tag::APP:
                this->app.~App();
                break;
            case Tag::ABS:
                this->abs.~Abs();
                break;
            default:
                assert(0);
        }
        this->tag.~Tag();
    }
	std::string stringify () {
		switch(this->tag) {
            case Tag::VOID:
                assert(0);
            case Tag::VAR:
                return this->var.name;
			case Tag::FUN:
			{
				std::string ret {};
				ret += this->fun.name;
				ret += "(";
				for ( auto& term : this->fun.list ) {
					ret += term->stringify();
					ret += ",";
				}
				ret += ")";
				return ret;
			}
            case Tag::APP:
			{
				std::string ret {};
				ret += "(" + this->app.left->stringify() + ")";
				ret += "(" + this->app.right->stringify() + ")";
				return ret;
			}
            case Tag::ABS:
			{
				std::string ret {};
                ret += "(\\_.";
				ret += this->abs.abstracted_term->stringify() + ")";
				return ret;
			}
            default:
                assert(0);
        }
	}
	void set_parent(TermVariant* p) {
		this->parent = p;
	}
	void set_parent(const Ptr<TermVariant>& p) {
		this->set_parent(p.get());
	}
	bool is_redex() const {
		return((this->tag == Tag::APP) && (this->app.left->tag == Tag::ABS));
	}
	class iterator {
			std::deque<TermVariant*> itr_stack;
			iterator () { }
		public:
			iterator (TermVariant* s);
			iterator& operator++();
			TermVariant* operator*();
			TermVariant* operator->();
			bool operator==(const iterator&) const;
			bool operator!=(const iterator&) const;
			static iterator end;
	};
	iterator begin() {
		return TermVariant::iterator{ this };
	}
	iterator end() {
		return TermVariant::iterator::end;
	}
	////
};

