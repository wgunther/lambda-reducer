#include "TermVariant.hh"

TermVariant::iterator::iterator (TermVariant* s)
	: itr_stack{s} { }
TermVariant::iterator TermVariant::iterator::end = iterator();

TermVariant::iterator& TermVariant::iterator::operator++() { // prefix
	TermVariant* current = this->itr_stack.back();
	this->itr_stack.pop_back();
	switch (current->tag) {
		case TermVariant::Tag::VOID:
			assert(0);
		case TermVariant::Tag::VAR:
			break;
		case TermVariant::Tag::FUN:
			for (auto& fun_arg : current->fun.list) {
				if (fun_arg)
					this->itr_stack.push_back(fun_arg.get());
			}
			break;
		case TermVariant::Tag::ABS:
			if (current->abs.abstracted_term)
				this->itr_stack.push_back(current->abs.abstracted_term.get());
			break;
		case TermVariant::Tag::APP:
			if (current->app.right)
				this->itr_stack.push_back(current->app.right.get());
			if (current->app.left)
				this->itr_stack.push_back(current->app.left.get());
			break;
	}
	return *this;
}

TermVariant* TermVariant::iterator::operator*() {
	return this->itr_stack.back();
}

TermVariant* TermVariant::iterator::operator->() {
	return this->itr_stack.back();
}

bool TermVariant::iterator::operator==(const TermVariant::iterator& i) const {
	if (this->itr_stack.size() == 0) {
		 if (i.itr_stack.size() == 0) {
			 return true;
		 }
		 return false;
	}
	if (i.itr_stack.size() == 0)
		return false;
	if (i.itr_stack.size() == this->itr_stack.size()
		&& i.itr_stack.back() == this->itr_stack.back()) {
		return true;
	}
	return false;
}

bool TermVariant::iterator::operator!=(const TermVariant::iterator& i) const {
	return !(*this == i);
}
