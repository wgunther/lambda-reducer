%{
#include <string>
#include "parser.tab.hh"
#include "lexer_class.hh"
using Token=FlexBison::Parser::token;
#define YY_NO_UNISTD_H
%}

%option noyywrap
%option yyclass="Lexer"
%option c++

%%
"\\"	{ return Token::LAMBDA; }
"("		{ return Token::BEGIN_GROUP; }
")"		{ return Token::END_GROUP; }
"let"	{ return Token::LET; }
"="		{ return Token::EQUAL; }
"in"	{ return Token::IN; }
"." { return Token::DOT; }
"," { return Token::COMMA; }
[a-z][a-zA-Z]* { yylval->string = new boost::flyweight<std::string>{std::string{yytext}}; return Token::VAR_NAME; }
[A-Z][a-zA-Z]* { yylval->string = new boost::flyweight<std::string>{std::string{yytext}}; return Token::FUN_NAME; }
[ \t\n] { }
%%
