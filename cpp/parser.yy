%skeleton "lalr1.cc"
%require "2.5"
%defines
%define namespace "FlexBison"
%define parser_class_name "Parser"

%code requires{
	#include <iostream>
	using std::cout; using std::endl;
	namespace FlexBison {
		class Lexer;
	}
#include "Term.hh"
}

%lex-param { Lexer &lexer }
%parse-param { Lexer &lexer }


%code {
#include "lexer_class.hh"
	static int yylex(FlexBison::Parser::semantic_type *yylval,
                    FlexBison::Lexer  &lexer);
}
%union {
	boost::flyweight<std::string>* string;
	std::list<std::unique_ptr<Term>>* termlist;
	Term* T;
}

%token LAMBDA BEGIN_GROUP END_GROUP LET EQUAL IN DOT COMMA ENDOFFILE
%token<string> VAR_NAME FUN_NAME

%type<termlist> varlist
%type<termlist> lambdalist
%type<T> letexpression
%type<T> lambdaterm
%type<T> dottedabstraction
%type<T> dotfreeterm
%type<T> atom




%%

data:
		letexpression 
	| 	lambdaterm {
			cout << $1->term->stringify() << endl;
			delete($1);
		} 
;

letexpression: 
		LET VAR_NAME EQUAL lambdaterm IN data {
			std::cout << "let expression" << std::endl;
		}
	|	LET FUN_NAME BEGIN_GROUP varlist END_GROUP EQUAL lambdaterm IN data {
			std::cout << "let fun expression" << std::endl;
		}
;

lambdalist:
		lambdaterm {
			$$ = new std::list<std::unique_ptr<Term>> {}; 
			$$->emplace_back(std::unique_ptr<Term>($1));
		}	
	| 	lambdalist COMMA lambdaterm {
			$1->emplace_back(std::unique_ptr<Term>($3));
		}
;

varlist:
		VAR_NAME {
			$$ = new std::list<std::unique_ptr<Term>> {}; 
			$$->emplace_back(std::unique_ptr<Term>{ new Term{ std::move(*$1) } }); 
			delete($1);
		}
	|	varlist COMMA VAR_NAME {
			$1->emplace_back(std::unique_ptr<Term>{ new Term{ std::move(*$3) } });
			delete($3);
			$$ = $1;
		}	
;	

lambdaterm:
		dotfreeterm {
			$$ = $1;
		}
	|	DOT lambdaterm {
			$$ = $2;
		}
	|	dotfreeterm DOT lambdaterm {
			$$ = new Term{ std::move(*$1), std::move(*$3) };
			delete($1);
			delete($3);
		}
	|	dottedabstraction {
			$$ = $1;
		}
;

dottedabstraction:
		LAMBDA VAR_NAME DOT lambdaterm {
			$$ = new Term{ std::move(*$2), std::move(*$4) };
			delete($2);
			delete($4);
		}
	|	LAMBDA VAR_NAME dottedabstraction {
			$$ = new Term{ std::move(*$2), std::move(*$3) };
			delete($2);
			delete($3);
		}
;

dotfreeterm:
		atom {
			$$ = $1;
		}
	|	dotfreeterm atom {
			$$ = new Term{ std::move(*$1), std::move(*$2) };
			delete($1);
			delete($2);
		}
;

atom:
		VAR_NAME { 
			$$ = new Term { std::move(*$1) };
			delete($1); 
		}
	|	FUN_NAME BEGIN_GROUP lambdalist END_GROUP {
			$$ = new Term { std::move(*$1), std::move(*$3) };
			delete($1);
			delete($3);
		}
	|	BEGIN_GROUP lambdaterm END_GROUP {
			$$ = $2;
		}	
	|	LAMBDA VAR_NAME atom {
			$$ = new Term{ std::move(*$2), std::move(*$3) };
			delete($2);
			delete($3);
		} 	
;

%%

void FlexBison::Parser::error (const location_type& blah, const std::string& error_message) {
	std::cerr << "Error (" << blah << "): " << error_message << std::endl;
}

static int yylex( FlexBison::Parser::semantic_type *yylval,
       FlexBison::Lexer &lexer)
{
   return( lexer.yylex(yylval) );
}
