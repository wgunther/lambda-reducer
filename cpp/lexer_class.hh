#ifndef __LEXER_CLASS_HH__
#define __LEXER_CLASS_HH__

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#undef YY_DECL
#define YY_DECL int FlexBison::Lexer::yylex()

namespace FlexBison {

struct Lexer : public yyFlexLexer {
	Lexer(std::istream *in, std::ostream *out)
		: yyFlexLexer{in, out}, yylval{nullptr} { }
	int yylex(FlexBison::Parser::semantic_type *lval) {
		yylval = lval;
		return yylex();
	}
private:
	int yylex();
	FlexBison::Parser::semantic_type *yylval;
};

}

#endif /* __LEXER_CLASS_HH__ */
