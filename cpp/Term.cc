#include "Term.hh"
using std::cout;
using std::endl;

TermVariant* Term::replace_from_parent_release(TermVariant* old_term, TermVariant* new_term) {
	if (!old_term || !old_term->parent)
		return nullptr;
	auto parent = old_term->parent;
	switch (parent->tag) {
		case TermVariant::Tag::VAR:
			assert(0);
			break;
		case TermVariant::Tag::FUN:
			for (auto& fun_arg : parent->fun.list) {
				if ( fun_arg.get() == old_term ) {
					old_term = fun_arg.release();
					fun_arg.reset(new_term);
					if (new_term)
						new_term->set_parent(parent);
                    old_term->set_parent(nullptr);
					return old_term;
				}
			}
			break;
		case TermVariant::Tag::APP:
			if (parent->app.left.get() == old_term) {
				old_term = parent->app.left.release();
				parent->app.left.reset(new_term);
				if (new_term)
					new_term->set_parent(parent);
				return old_term;
			} else if (old_term->parent->app.right.get() == old_term) {
				old_term = parent->app.right.release();
				parent->app.right.reset(new_term);
				if (new_term)
					new_term->set_parent(parent);
                old_term->set_parent(nullptr);
				return old_term;
			}
			break;
		case TermVariant::Tag::ABS:
			if (old_term->parent->abs.abstracted_term.get() == old_term) {
				old_term = parent->abs.abstracted_term.release();
				parent->abs.abstracted_term.reset(new_term);
				if (new_term)
					new_term->set_parent(parent);
                old_term->set_parent(nullptr);
				return old_term;
			}
			return nullptr;
		default:
			assert(0);
			break;
	}
	assert(0);
}
void Term::replace_from_parent_reset(TermVariant* old_term, TermVariant* new_term) {
	old_term = this->replace_from_parent_release(old_term, new_term);
	this->destroy_all_variables(old_term); //XXX: yes?
	delete(old_term);
}

void Term::substitute_in_place(TermVariant* old_term, TermVariant* new_term) {
	auto parent = old_term->parent;
	if (parent == nullptr) {
		assert(old_term == this->term.get());
		// new_term's parenthood will change.
		// this saves it from getting reaped when old_term dies
		// if new_term happens to be a subterm.
        this->replace_from_parent_release(new_term, nullptr);
		this->destroy_all_variables(old_term); //XXX: yes?
		this->term.reset(new_term);
	} else {
		// new_term's parenthood will change.
		// this saves it from getting reaped when old_term dies
		// if new_term happens to be a subterm.
        replace_from_parent_release(new_term, nullptr);
		this->replace_from_parent_reset(old_term, new_term);
	}
}

auto Term::get_redexes() -> list<TermVariant*>  {
	list<TermVariant*> l;
	for (auto subterm : *this->term) {
		if (subterm->is_redex()) {
			l.push_back(subterm);
		}
	}
	return l;
}

void Term::destroy_all_variables(TermVariant* t) {
	for (auto subterm : *t) {
		if ( t->tag == TermVariant::Tag::VAR ) {
			if (auto binding_term = subterm->var.bound_at) {
				binding_term->abs.bound_vars.remove(t);
			} 
			else {
				auto itr = this->fv.find(t->var.name);
				while (itr != std::end(this->fv)) { //XXX: Is that how iterators work?
					if (itr->second == subterm) {
						this->fv.erase(itr);
						break;
					}
				}
				++itr;
				assert(0);
			}
		}
	}
}

bool Term::contract_redex(TermVariant* redex) {
	if (! redex->is_redex() )
		return false;
	auto contextHoles = redex->app.left->abs.bound_vars;
	auto itr = contextHoles.begin();
	if (itr != contextHoles.end()) {
		// I-redex
		auto copyingTerm = redex->app.right.get();
		this->substitute_in_place(*itr, copyingTerm);
		++itr;
		while (itr != contextHoles.end()) {
			auto clone = this->clone_with_side_effects(copyingTerm);
			this->substitute_in_place(*itr, clone);
			++itr;
		}
	} 
	/*
		// Don't think I have to do this here because I destroy stuff when substituting.
		else {
			// K-redex
			this->destroy_all_variables(redex->app.right.get());
		}
	*/
	this->substitute_in_place(redex, redex->app.left->abs.abstracted_term.get());
	return true;
}

TermVariant* Term::clone_with_side_effects(TermVariant* old_term) {
	switch ( old_term->tag ) {
		case TermVariant::Tag::VAR: {
			auto new_term = new TermVariant{ Var{old_term->var.name} };
			if ( old_term->var.bound_at != nullptr ) {
				old_term->var.bound_at->abs.bound_vars.push_front(new_term);
				new_term->var.bound_at = old_term->var.bound_at;
			}
			else {
				this->fv.insert(pair<string,TermVariant*>{new_term->var.name, new_term});
            }
			return new_term;
		} case TermVariant::Tag::FUN: {
			auto subterm_list = new TermVariant::List<Ptr<TermVariant>>{};
			auto itr = subterm_list->before_begin();
			for ( auto& term : old_term->fun.list) {
				subterm_list->emplace_after(itr,this->clone_with_side_effects(term.get()));
				++itr;
			}
			auto new_term = new TermVariant{ Fun{old_term->fun.name, std::move(*subterm_list) } };
			return new_term;
		} case TermVariant::Tag::APP: {
			auto new_left = this->clone_with_side_effects(old_term->app.left.get());
			auto new_right = this->clone_with_side_effects(old_term->app.right.get());
			auto new_term = new TermVariant{ App{ new_left, new_right} };
            return new_term;
		} case TermVariant::Tag::ABS: {
			auto backup_list = TermVariant::List<TermVariant*>(std::move(old_term->abs.bound_vars));
			new (&old_term->abs.bound_vars) TermVariant::List<TermVariant*>{};
			auto new_term = new TermVariant{ Abs{ this->clone_with_side_effects(old_term->abs.abstracted_term.get()) } };
			new (&new_term->abs.bound_vars) TermVariant::List<TermVariant*>{std::move(backup_list)};
			std::swap(new_term->abs.bound_vars, old_term->abs.bound_vars);
			return new_term;
		} default:
			assert(0);
	}
}
