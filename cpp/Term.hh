#include "TermVariant.hh"
#include <map>
#include <unordered_map>
#include <utility>
#include <list>



class Term {
using string = typename boost::flyweight<std::string>;
template <typename T1, typename T2>
using pair = typename std::pair<T1, T2>;
template <typename T>
using list = typename std::list<T>;
template <typename T>
using Ptr = typename TermVariant::Ptr<T>;
using LookupTable = typename std::multimap<string,TermVariant*>;
	public:
		Ptr<TermVariant> term;
		LookupTable fv;
	private:
	public:
		// Var constructors
		Term(string&& varname) {
			TermVariant* var = new TermVariant(Var{std::move(varname)});
			this->term.reset(var);
			this->fv.insert(
				pair<string, TermVariant*>{var->var.name, var}
			);
		}
		Term(const char* varname) : Term{ string{std::string{varname}} } { }
		// Fun constructors
		Term(string funName, list<Ptr<Term>>&& l) {
			using std::cout;
			using std::endl;
			TermVariant* fun = new TermVariant(Fun{std::move(funName)});
			TermVariant::List<Ptr<TermVariant>> newlist; 
			for ( auto termInList_Itr = l.rbegin(); 
				  termInList_Itr != l.rend(); 
				  ++termInList_Itr ) {
				auto termInList = (*termInList_Itr)->term.release();
				termInList->set_parent(fun);
				newlist.emplace_front(termInList);
				this->fv = mergeMultmap(
					std::move(this->fv), std::move((*termInList_Itr)->fv)
				);
				//l.erase((++termInList_Itr).base()); //XXX: I think?
			}
			fun->fun.set_list(std::move(newlist));
			this->term.reset(fun);
			this->fv.insert(
				pair<string, TermVariant*>{fun->fun.name, fun}
			);
		}
		Term(const char* funName, list<Ptr<Term>>&& l) : Term{ string{std::string{funName}}, std::move(l) } { }
		// Abs constructors
		Term(string varname, Term&& t) 
		: term{new TermVariant{Abs{t.term.release()}}}, 
		  fv{std::move(t.fv)}
		{
			auto range = this->fv.equal_range(varname);
			for ( auto newBoundVariablePair = range.first; 
				  newBoundVariablePair != range.second; 
				  ++newBoundVariablePair ) {
				newBoundVariablePair->second->var.bound_at = this->term.get();
				this->term->abs.bound_vars.push_front(
					std::move(newBoundVariablePair->second)
				);
			}
			this->fv.erase(varname);
			this->term->abs.abstracted_term->set_parent(this->term);
		}
		Term (const char* varname, Term&& t)
		: Term{ string{std::string{varname}}, std::move(t)} { }
		// App constructors
		Term(Term&& l, Term&& r) 
		: term{new TermVariant{App{ l.term.release(), r.term.release() }}}, 
		  fv{ mergeMultmap(std::move(l.fv), std::move(r.fv)) } 
		{ 
			this->term->app.left->set_parent(this->term);
			this->term->app.right->set_parent(this->term);
		}
		//
		list<TermVariant*> get_redexes();
		bool contract_redex(TermVariant*);
	private:
		void substitute_in_place(TermVariant*,TermVariant*);
		TermVariant* clone_with_side_effects(TermVariant*);
		void destroy_all_variables(TermVariant*);
		TermVariant* replace_from_parent_release(TermVariant*, TermVariant*);
		void replace_from_parent_reset(TermVariant*, TermVariant*);
	// static:
		static inline LookupTable mergeMultmap (LookupTable map1, LookupTable map2) {
			if (map1.size() > map2.size()) {
				for (auto keyval : map2) {
					map1.insert(keyval);
				}
				return map1;
			} else {
				for (auto keyval : map1) {
					map2.insert(keyval);
				}
			return map2;
			}
		}
};
