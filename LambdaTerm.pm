package LambdaTerm;
use v5.16;
use Data::Dumper 'Dumper';
no warnings 'experimental';
my $dummy = LambdaTerm->new('Var', 'X');

sub new { shift; # class method.
	# Takes in:
	#	'Var', string VarName
	#	'Fun', string FunName, array of LambdaTerms (instList)
	#	'Abs', LambdaTerm Var, LambdaTerm Term
	#	'App', LambdaTerm Left, LambdaTerm Right 
	# Returns: LambdaTerm (acting as head to term).
	my $type = shift;
	my $self = bless { type => $type, fv => {} };
	given($type) {
		when ('Var') {
			my ($varName) = @_;
			$self->{name} = $varName;
			$self->{fv}{$varName} = [$self];
		}
		when ('Fun') {
			my ($funName, $lamList) = @_;
			$self->{name} = $funName;
			$self->{arity} = scalar(@$lamList);
			$self->{fv}{$funName} = [$self];
			# Set children
			$self->{list} = $lamList;
		}
		when ('Abs') {
			my ($var, $term) = @_;
			my $varName = $var->{name};
			my @absPlaces = $term->{fv}{$varName} ? @{$term->{fv}{$varName}} : ();
			delete $term->{fv}{$varName}; # It is no longer free.
			
			# Tell bound variables where they are bound.
			$self->{bound_vars} = \@absPlaces;
			foreach my $boundVariable ( @absPlaces ) {
				$boundVariable->{bound_at} = $self;
			}
			
			# Set children
			$self->{term} = $term;
		}
		when ('App') {
			my ($left, $right) = @_;
			# Set children
			$self->{left} = $left;
			$self->{right} = $right;
		}
	}
	foreach my $child ($self->children) {
		$child->{parent} = $self;
		# Collect free variables.
		foreach my $freeVar (keys %{$child->{fv}}) {
			if ($self->{fv}{$freeVar}) {
				# Merge what we have if we have anything
				push @{$self->{fv}{$freeVar}}, @{$child->{fv}{$freeVar}};
			}
			else {
				# Reuse what we had if we had nothing.
				$self->{fv}{$freeVar} = $child->{fv}{$freeVar};
			}
		}
		# Children free variable will be invalidated during reduction
		# So we delete them now.
		delete($child->{fv});
	}
	return $self;
}

sub isRedex { my $self = shift;
	return ($self->{type} eq 'App') && ($self->{left}{type} eq 'Abs');
}

sub children { my $self = shift;
	given ($self->{type}) {
		return () when ('Var');
		return @{$self->{list}} when ('Fun');
		return ($self->{term}) when ('Abs');
		return ($self->{left}, $self->{right}) when ('App');
	}
}

sub redexes { my $self = shift;
	# Takes in: void
	# Returns: array of redex reference in leftmost/outmost order.
	my @redexes = ();
	my @stack = ($self);
	while (@stack) {
		my $subterm = shift @stack;
		if ($subterm->isRedex) {
			push @redexes, $subterm;
		}
		unshift @stack, $subterm->children;
	}
	return @redexes;
}

sub contractRedex { my $self = shift;
	my ($head) = @_;
	# Takes in: head (optional if the method is head).
	# Returns: head (possibly changed).
	if ($self->isRedex) {
		$head = $self if ! $head;
		my $appliciant = $self->{left};
		my $substituent = $self->{right};
		my @contextHoles = @{$appliciant->{bound_vars}};
		
		# Perform substitution.
		if (my $hole = shift(@contextHoles)) {
			# I-redex
			$head = $hole->substituteInPlace($head, $substituent);	
			while (@contextHoles) {
				$hole = shift(@contextHoles);
				my $clone = $substituent->cloneWithSideEffects($head);
				delete($clone->{fv});
				$head = $hole->substituteInPlace($head, $clone);
			}
		} else {
			# K-redex 
			# clean up free/bound variables
			$substituent->destroyAllVariables($head);
			undef(%{$substituent});
		}
		$head = $self->substituteInPlace($head, $appliciant->{term} );
		return $head;
	}
	return 0;
}

sub substituteInPlace { my $self = shift;
	my ($head,$new) = @_;
	# Takes in: head, new term.
	# Returns: head (possibly changed).
	# Purpose
	# 	the term in method is replaces by new (with possible head change)
	if (! $self->replaceFromParent($new)) {
		$new->{fv} = $head->{fv};
		undef(%{$head});
		$head = $new;
		delete($head->{parent});
	}
	return $head;
}
sub replaceFromParent { my $self = shift;
	# Takes in: new
	# Returns: 1 if successful, 0 otherwise. 
	# Purpose 
	# 	if term doesn't have parent, fails
	# 	parent now points to new in the location it used to point to self	
	# 	new->parent points to self->parent
	my ($new) = @_;
	my $parent = $self->{parent};
	if ($parent) {
		given ($parent->{type}) {
			when ('Fun') {
				for (my $i = 0; $i < scalar($parent->{list}); ++$i) {
					if ($parent->{list}[$i] == $self) {
						$new->{parent} = $parent;
						undef(%{$parent->{list}[$i]});
						$parent->{list}[$i] = $new;
						return 1;
					}
				}
			}
			when ('Abs') {
				if ($parent->{term} == $self) {
					$new->{parent} = $parent;
					undef(%{$parent->{term}});
					$parent->{term} = $new;
					return 1;
				}
			}
			when ('App') {
				if ($parent->{left} == $self) {
					$new->{parent} = $parent;
					undef(%{$parent->{left}});
					$parent->{left} = $new;
					return 1;
				} elsif ($parent->{right} == $self) {
					$new->{parent} = $parent;
					undef(%{$parent->{right}});
					$parent->{right} = $new;
					return 1;
				}
			}
		}
	}
	return 0;
}

sub cloneWithSideEffects { my $self = shift;
	my ($head) = @_;
	# Takes in: head term (optional: self if not given).
	# Returns: cloned subterm of object, where the variables mentioned
	# are registered in the correct places in the superterm. 
	# So it registers the cloned variables are bound where
	# the old ones where if the old ones live outside the scope of the first
	# term given to the function (so it preserves all substructure, but affects
	# superstructure, if that makes sense). 
	$head = $self if ! $head;
	my $newSelf;
	given ($self->{type}) {
		when ('Var') {
			my $varName = $self->{name};
			$newSelf = $self->new('Var', $varName);
			if ($self->{bound_at}) {
				# Register your boundness. This is a way of the variable
				# communicating with its abstractor; to see why, see the
				# abstraction case. 
				#
				# In the event that the term is not bound 'internally'
				# this will still register, which is the effect we want, since
				# this is a clone "with side effects" 
				push @{$self->{bound_at}{bound_vars}}, $newSelf;
				$newSelf->{bound_at} = $self->{bound_at};
			}
			else {
				# Register your freeness.
				$head->{fv}{$varName} = [] if ! $head->{fv}{$varName};
				push @{$head->{fv}{$varName}}, $newSelf;
			}
		}
		when ('Fun') {
			my $funName = $self->{name};
			# Recurse
			my @lamList = map { $_->cloneWithSideEffects($head); } $self->children;
			$newSelf = $self->new('Fun', $funName, \@lamList);
			# Register the variable name.
			$head->{fv}{$funName} = [] if ! $head->{fv}{$funName};
			push @{$head->{fv}{$funName}}, $newSelf;
		}
		when ('Abs') {
			# When I do this recursively, all the bound variables bound at this point 
			# are going to register themselves as bound by pointing at $self.
			# So we exploit that by finding all of the bound variables by 'catching'
			# them using this term. But we have to fix it after we're done
			my $backupBound = $self->{bound_vars};
			$self->{bound_vars} = [];
			# Recurse
			my $term = $self->{term}->cloneWithSideEffects($head);
			my $var = @{$self->{bound_vars}} ? $self->{bound_vars}[0] : $dummy;
			my $varName = $var->{name};
			# Register the bound variables we caught as free variables;
			# this is what the constructor for abstractions is expecting.
			# We also de-register the bound quality of the bound variables.
			$term->{fv}{$varName} = $self->{bound_vars};
			# I don't think this code is necessary since this will be handled
			# in the constructor for the abstraction.
			#foreach my $notYetBoundVar (@{$term->{fv}{$varName}}) {
			#	delete($notYetBoundVar->{bound_at});
			#}

			# Here we restore the bound variables to the original term.
			$self->{bound_vars} = $backupBound;
			$newSelf = $self->new('Abs', $var, $term);
		}
		when ('App') {
			# Recurse
			my ($left, $right) = map { $_->cloneWithSideEffects($head); } $self->children;
			$newSelf = $self->new('App', $left, $right);
		}
	}
	return $newSelf;
}


sub destroyAllVariables { my $self = shift;
	my ($head) = @_;
	# Takes in: head variable
	# Returns: void
	# Purpose:
	# 	essentially does destroyFreeVariables and destroyBoundVariables
	# 	just at the same time, so it doesn't have to be scanned twice.
	my @stack = ($self);
	while (@stack) {
		my $subterm = pop @stack;
		if ($subterm->{type} eq 'Var') {
			if (my $binding_term = $subterm->{bound_at}) {
				@{$binding_term->{bound_vars}} = grep { $_ != $subterm } @{$binding_term->{bound_vars}};
				delete($subterm->{bound_at});
			} else {
				my $varName = $subterm->{name};
				@{$head->{fv}{$varName}} = grep { $_ != $subterm } @{$head->{fv}{$varName}};
				delete($head->{fv}{$varName}) if ! @{$head->{fv}{$varName}};
			}
		}
		push @stack, $subterm->children;
	}
}

sub destroyFreeVariables { my $self = shift;
	my ($head) = @_;
	# Takes in: head variable
	# Returns: void
	# Purpose
	# 	Any free variable of the object will be deleted in the head registry. 
	my @stack = ($self);
	while (@stack) {
		my $subterm = pop @stack;
		if ($subterm->{type} eq 'Var') {
			my $varName = $subterm->{name};
			if ($head->{fv}{$varName}) {
				@{$head->{fv}{$varName}} = grep { $_ != $subterm } @{$head->{fv}{$varName}};
				delete($head->{fv}{$varName}) if ! @{$head->{fv}{$varName}};
			}
		}
		push @stack, $subterm->children;
	}
}

sub destroyBoundVariables { my $self = shift;
	my ($head) = @_;
	# Takes in: head variable
	# Returns: void
	# Purpose
	# 	Any bound variable will stop being bound.
	# 	by removing it's bound_at and removing it from bound_var in
	# 	its binding spot	
	my @stack = ($self);
	while (@stack) {
		my $subterm = pop @stack;
		if ($subterm->{type} eq 'Var') {
			if (my $binding_term = $subterm->{bound_at}) {
				@{$binding_term->{bound_vars}} = grep { $_ != $subterm } @{$binding_term->{bound_vars}};
				delete($subterm->{bound_at});
			}
		}
		push @stack, $subterm->children;
	}
}

sub string { my $self = shift;
	# Takes in: void
	# Returns: string
	my ($variableRestry) = @_;
	if (! $variableRestry) {
		$variableRestry = {};
	}
	given ($self->{type}) {
		when ('Var') {
			my $varName = $self->{name};
			# just so numbers work like you'd expect: 
			# 	just for alpha renaming
			$varName =~ /([a-z]+)\d*/;
			$varName = $1;
			if ($self->{bound_at}) {
				return $varName . $self->{bound_at}{print_name};
			} else {
				return $varName;
			}
		}
		when ('Fun') {
			my $funName = $self->{name};
			return "$funName(". join(', ', map{ $_->string($variableRestry); } @{$self->{list}}) .")";
		}
		when ('Abs') {
			my $varName = @{$self->{bound_vars}} ? $self->{bound_vars}[0]{name} : '_';
			# just so numbers work like you'd expect: 
			# 	just for alpha renaming
			$varName =~ /([a-z_]+)\d*/;
			$varName = $1;
			++$variableRestry->{$varName};
			$self->{print_name} = $variableRestry->{$varName};
			my $string = $self->{term}->string($variableRestry);
			$string = "λ$varName".$self->{print_name}.".$string";
			--$variableRestry->{$varName};
			delete($self->{print_name});
			return $string;
		}
		when ('App') {
			my $string1 = $self->{left}->string($variableRestry);
			my $string2 = $self->{right}->string($variableRestry);
			my $paren = 0;
			if ($self->{left}{type} eq 'Abs') {
				$string1 = '(' . $string1 . ')';
				$paren = 1;
			}
			if ($self->{right}{type} eq 'Abs'
				|| $self->{right}{type} eq 'App') {
				$string2 = '(' . $string2 . ')';
				$paren = 1;
			}

			if ($paren) {
				return "$string1$string2";
			} else {
				return "$string1 $string2";
			}
		}
	}
}
